import Link from 'next/link'
import React from 'react'
import { Container, Row, Col, Button, Jumbotron, ListGroup, ListGroupItem } from 'reactstrap'
import Page from '../components/page'
import Layout from '../components/layout'

export default class extends Page {
  render() {
    return (
      <Layout {...this.props} navmenu={false} fluid={true} container={false}>
        <Container fluid={true} className="marginTop">
          <Row>
            <Col sm={5}>
            <h2 className="display-4 mt-5 mb-2">Houseology - Privacy Policy</h2>
            <p className="lead">We take your privacy seriously</p>
            <h2>Privacy Policy</h2>
            <p>Your privacy is important to us. It is Houseology Live's policy to respect your privacy regarding any information we may collect from you across our website, <a href="https://www.houseologylive.co.uk">https://www.houseologylive.co.uk</a>, and other sites we own and operate.</p>
            <p>We only ask for personal information when we truly need it to provide a service to you. We collect it by fair and lawful means, with your knowledge and consent. We also let you know why we’re collecting it and how it will be used.</p>
            <p>We only retain collected information for as long as necessary to provide you with your requested service. What data we store, we’ll protect within commercially acceptable means to prevent loss and theft, as well as unauthorised access, disclosure, copying, use or modification.</p>
            <p>We don’t share any personally identifying information publicly or with third-parties, except when required to by law.</p>
            <p>Our website may link to external sites that are not operated by us. Please be aware that we have no control over the content and practices of these sites, and cannot accept responsibility or liability for their respective privacy policies.</p>
            <p>You are free to refuse our request for your personal information, with the understanding that we may be unable to provide you with some of your desired services.</p>
            <p>Your continued use of our website will be regarded as acceptance of our practices around privacy and personal information. If you have any questions about how we handle user data and personal information, feel free to contact us.</p>
            <p>This policy is effective as of 17 March 2019.</p>

            <h2>How can I remove my account with Houseology?</h2>
            <p>This can be done easily, you can delete your account within your account settings, we also give you the ability to unlink your social media accounts from Houseology Live</p>
            <p>We will not use, sell or re-distribute your private information, we solely use it to monitor and regulate our live chatting feature</p>
            </Col>
          </Row>
        </Container>
      </Layout>
    )
  }
}