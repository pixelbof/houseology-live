import Link from 'next/link'
import React from 'react'
import { Container, Row, Col, Button, Jumbotron, ListGroup, ListGroupItem } from 'reactstrap'
import Page from '../components/page'
import Layout from '../components/layout'

export default class extends Page {
  render() {
    return (
      <Layout {...this.props} navmenu={false} fluid={true} container={false}>
        <Container fluid={true}>
        <Row>
          <Col sm={12} md={5}>
            <h2 className="display-4 mt-5 mb-2">Houseology Podcasts</h2>
            <p>Welcome to our podcast zone, this is where you can listen to our 30 newest podcasts.</p>

            <p>With our podcasts you cantch up on the shows you have missed through the week, or catch one of the DJ's midweek mixes</p>

            <p>Keeping you entertained on the train, at work or in the car, you can also download and subscribe to our podcasts with iTunes, TuneIn or your own podcast system.</p>
          </Col>

          <Col sm={12} md={7}>
            <iframe id="multi_iframe" frameBorder="0" scrolling="false" allowFullScreen src="https://www.podbean.com/media/player/multi?playlist=http%3A%2F%2Fplaylist.podbean.com%2F1356951%2Fplaylist_multi.xml&vjs=1&kdsowie31j4k1jlf913=0f23b4793b01ab456c4b8d94f7ebafdb036dc41a&size=240&skin=12&auto=0&share=1&fonts=Helvetica&download=1&rtl=0&pbad=1" width="100%" height="430"></iframe>
          </Col>
        </Row>

        </Container>
        <script dangerouslySetInnerHTML={{__html: '(adsbygoogle = window.adsbygoogle || []).push({google_ad_client: "ca-pub-2953984828935412",enable_page_level_ads: true});'}}></script>
      </Layout>
    )
  }
}