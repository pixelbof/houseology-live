import Link from 'next/link'
import React from 'react'
import { Container, Row, Col, Button, Jumbotron, ListGroup, ListGroupItem } from 'reactstrap'
import Page from '../components/page'
import Layout from '../components/layout'

export default class extends Page {
  render() {
    return (
      <Layout {...this.props} navmenu={false} fluid={true} container={false}>
        <Container fluid={true}>
          <Row>
            <Col sm={5}>
            <h2 className="display-4 mt-5 mb-2">Welcome to Houseology</h2>
            <p>A musical collaboration of four digital music artist, DJs and Producers. MiKE RiCHARDS, The ScullyMan, S.Key &amp; Pro-lifik.</p>

            <p>The brain child of two close friends MiKE &amp; The ScullyMan having known each other for a little over 26 years, decided to start the artist Group &amp; Brand back in 2012 we now know today.</p>

            <p>Having been involved from the very start kicking off their Dj careers from the early 1990’s Rave Scene, both artists decided to start Houseology dedicated to the spirit and global love of music. Both lovers of filthy beats, underground breaks designed to rebel against the system and big room uplifting vibes, they set out to bring a hint of the 90s Rave and underground sub culture &amp; Splice it with today’s modern music arena. Having formally founded the brand in 2012 over the following 2 years would see the brand grow from strength to strength when finally in 2015, two new artists joined the group. Djs S.Key &amp; Pro-lifik! Exploding Houseology to the next level pushing the brand to what we now know it to be today. A High energy artist group with a passion for rebellious music ever pushing the boundaries of music. Today Houseology is dedicated to the spirit and love of music and can be seen on tour in Ayia Napa, Festivals and Ministry Of Sound with The Castle.</p>

            <p>Catch us live each and every Sunday, full of banter and fun, whilst keeping the integrity of music.</p>
              <Row>
                <Col sm={12}>
                  <img src={'../static/images/mike-scully.jpg'} style={{width:'23%', marginRight:'2%'}} />
                  <img src={'../static/images/reece-prolifik.jpg'} style={{width:'23%', marginRight:'2%'}} />
                  <img src={'../static/images/mike-skey.jpg'} style={{width:'23%', marginRight:'2%'}} />
                  <img src={'../static/images/the-crew.jpg'} style={{width:'23%'}} />
                </Col>
              </Row>
            </Col>

            <Col sm={7}>
              <img src={'../static/images/Houseology-live-crew.jpg'} style={{width:'100%'}} className="d-none d-lg-block" />
            </Col>
          </Row>
        </Container>
        <script dangerouslySetInnerHTML={{__html: '(adsbygoogle = window.adsbygoogle || []).push({google_ad_client: "ca-pub-2953984828935412",enable_page_level_ads: true});'}}></script>
      </Layout>
    )
  }
}