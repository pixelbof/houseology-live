import Link from 'next/link'
import React from 'react'
import { Container, Row, Col, Button, Jumbotron, ListGroup, ListGroupItem } from 'reactstrap'
import Page from '../components/page'
import Layout from '../components/layout'

export default class extends Page {
  render() {
    return (
      <Layout {...this.props} navmenu={false} fluid={true} container={false}>
      <Container fluid={true} className="marginTop">
      <Row>
        <Col sm={12} md={5} className="border-right ml-10 pl-10">
          <h2 className="display-4 mt-5 mb-2">Whats happening with Houseology?</h2>
          <p>This is a question we get everyday, so now you can follow where we are, what we are doing and most importantly, where you can meet us!</p>
          <p>We post our event, outings and away days here.</p>
          <p className="lead">We like it when we keep you updated!</p>
        </Col>

        <Col sm={12} md={6} className="text-right">
         <h2 className="display-4 mt-5 mb-2">Our upcoming events</h2>
          <hr />
         <Row>
           <Col sm={12} className="text-right">
             <p className="lead">Houseology - Ministry of Sound <strong>Fri 16 Aug 2019</strong></p>
             <img src={'../static/images/MOS_event_160819.jpg'} />
            </Col>
         </Row>

        </Col>
      </Row>

      </Container>
      <script dangerouslySetInnerHTML={{__html: '(adsbygoogle = window.adsbygoogle || []).push({google_ad_client: "ca-pub-2953984828935412",enable_page_level_ads: true});'}}></script>
    </Layout>
    )
  }
}