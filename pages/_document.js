import Document, { Head, Main, NextScript } from 'next/document'

export default class extends Document {
  render() {
    /**
    * Here we use _document.js to add a "lang" propery to the HTML object if
    * one is set on the page.
    **/
    return (
      <html lang={this.props.__NEXT_DATA__.props.pageProps.lang || 'en'}>
        <Head>
          <link rel="icon" type="image/x-icon" href="../static/favicons/favicon.ico" />
          <link rel="apple-touch-icon" sizes="180x180" href="../static/favicons/apple-touch-icon.png" />
          <link rel="icon" type="image/png" sizes="32x32" href="../static/favicons/favicon-32x32.png" />
          <link rel="icon" type="image/png" sizes="16x16" href="../static/favicons/favicon-16x16.png" />
          <link rel="mask-icon" href="../static/favicons/safari-pinned-tab.svg" color="#5bbad5" />
        </Head>
        <body>
          <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
          <Main />
          <NextScript />
        </body>
      </html>
    )
  }
}