import React from 'react'
import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption } from 'reactstrap'
import Page from '../components/page'
import Layout from '../components/layout'

const items = [
  {
    src: '/static/images/dj-mike-richards.jpg',
    altText: 'DJ Mike Richards',
    caption: 'Mike Richards, smashing out the tunes week after week'
  },
  {
    src: '/static/images/dj-scullyman.jpg',
    altText: 'DJ Scullyman',
    caption: 'DJ Scullyman, Giving you the tunes each and every week'
  },
  {
    src: '/static/images/dj-prolifik-2.jpg',
    altText: 'DJ Pro-lifik',
    caption: 'DJ Pro-Lifik, #IanDidIt'
  },
  {
    src: '/static/images/dj-skey.jpg',
    altText: 'DJ S-Key',
    caption: 'DJ S-Key, the smile that sank a thousand ships, the siren of sound'
  },
  {
    src: '/static/images/party-boat.jpg',
    altText: 'Fantasy Boat Party',
    caption: 'All aboard the Fantasy Boat Party, the #1 party boat in Ayia Napa'
  },
  {
    src: '/static/images/castle-club-1.jpg',
    altText: 'The Castle Club',
    caption: 'The Castle Club, Ayia Napa, catch Houseology here! You know you want to'
  },
  {
    src: '/static/images/castle-club-2.jpg',
    altText: 'The Castle Club',
    caption: 'We love it so much we\'re showing you it twice!'
  }
];

export default class extends Page {
  constructor(props) {
    super(props);
    this.state = { activeIndex: 0 };
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);
  }

  onExiting() {
    this.animating = true;
  }

  onExited() {
    this.animating = false;
  }

  next() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === items.length - 1 ? 0 : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  }

  previous() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === 0 ? items.length - 1 : this.state.activeIndex - 1;
    this.setState({ activeIndex: nextIndex });
  }

  goToIndex(newIndex) {
    if (this.animating) return;
    this.setState({ activeIndex: newIndex });
  }

  render() {
    const { activeIndex } = this.state;

    const slides = items.map((item) => {
      return (
        <CarouselItem
          onExiting={this.onExiting}
          onExited={this.onExited}
          key={item.src}
        >
          <img src={item.src} alt={item.altText} style={{width: '100%'}} />
          <CarouselCaption captionText={item.caption} captionHeader={item.altText} />
        </CarouselItem>
      );
    });
    
    return (
      <Layout {...this.props} navmenu={false} fluid={true} container={false}>
        <Carousel
          activeIndex={activeIndex}
          next={this.next}
          previous={this.previous}
        >
          <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={this.goToIndex} />
          {slides}
          <CarouselControl direction="prev" directionText="Previous" onClickHandler={this.previous} />
          <CarouselControl direction="next" directionText="Next" onClickHandler={this.next} />
        </Carousel>
      </Layout>
    )
  }
}