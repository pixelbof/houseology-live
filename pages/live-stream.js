import Link from 'next/link'
import React from 'react'
import { Container, Row, Col, Button, Jumbotron, ListGroup, ListGroupItem } from 'reactstrap'
import Page from '../components/page'
import Layout from '../components/layout'
import IframeComponent from '../components/iframe'
import Chat from '../components/chat'

export default class extends Page {
  render() {
    return (
      <Layout {...this.props} navmenu={false} fluid={true} container={false}>
        <Container fluid={true}>
          <h2 className="display-4 mt-5 mb-2">Houseology Live TV</h2>
          <Row className="pb-5">
            <Col md="8" sm="12" id="livePlayer" className="videoZone" style={{position:'relative',width:'100%',overflow:'hidden'}}>
                {<IframeComponent/>}
            </Col>

            <Chat session={this.props.session} />
            <a href="https://www.patreon.com/bePatron?u=19102988" data-patreon-widget-type="become-patron-button">< img src="../static/images/become_a_patron.png" width="200px" style={{margin: '6px 0 10px 14px'}} /></a>
          </Row>
        </Container>
        <script dangerouslySetInnerHTML={{__html: '(adsbygoogle = window.adsbygoogle || []).push({google_ad_client: "ca-pub-2953984828935412",enable_page_level_ads: true});'}}></script>
      </Layout>
    )
  }
}