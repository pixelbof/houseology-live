import React from 'react'
import Router from 'next/router'
import Head from 'next/head'
import Link from 'next/link'
import NProgress from "next-nprogress/component";
import { Container, Row, Col, Nav, NavItem, Button, Form, NavLink, Collapse,
         Navbar, NavbarToggler, NavbarBrand, Modal, ModalHeader, ModalBody,
         ModalFooter, ListGroup, ListGroupItem } from 'reactstrap'
import Signin from './signin'
import { NextAuth } from 'next-auth/client'
import Cookies from 'universal-cookie'
import Package from '../package'
import Styles from '../css/index.scss'

export default class extends React.Component {

  static propTypes() {
    return {
      session: React.PropTypes.object.isRequired,
      providers: React.PropTypes.object.isRequired,
      children: React.PropTypes.object.isRequired,
      fluid: React.PropTypes.boolean,
      navmenu: React.PropTypes.boolean,
      signinBtn: React.PropTypes.boolean
    }
  }
  
  constructor(props) {
    super(props)
    this.state = {
      navOpen: false,
      modal: false,
      providers: null
    }
    
    this.toggleModal = this.toggleModal.bind(this);
  }
  
  async toggleModal(e) {
    if (e) e.preventDefault()

    // Save current URL so user is redirected back here after signing in
    if (this.state.modal !== true) {
      const cookies = new Cookies()
      cookies.set('redirect_url', window.location.pathname, { path: '/' })
    }

    this.setState({
      providers: this.state.providers || await NextAuth.providers(),
      modal: !this.state.modal
    })
  }
  
  render() {
    return (
      <React.Fragment>
        <Head>
          <meta charSet="UTF-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1"/>
          <meta name="Title" content="Houseology Live - Sundays 6pm till 10pm" />
          <meta name="Keywords" content="Houseology, Houseology Live, Music, Live, House, Trance, Ayia Napa, Ministry of Sound, mike richards, scullyman, pro-lifik, s.key, sundays " />
          <meta name="Description" content="A musical collaboration of four digital music artist, DJs and Producers. MiKE RiCHARDS, The ScullyMan, S.Key & Pro-lifik. Live Sundays 6pm till 10pm (GMT)" />
          <meta name="Subject" content="Houseology Live - Sundays 6pm till 11pm (GMT)" />
          <meta name="Language" content="United Kingdom" />
          <meta name="Robots" content="INDEX,FOLLOW" />

          <meta property="og:title" content="Houseology Live - Sundays 6pm till 10pm" />
          <meta property="og:description" content="A musical collaboration of four digital music artist, DJs and Producers. MiKE RiCHARDS, The ScullyMan, S.Key & Pro-lifik. Live Sundays 6pm till 10pm (GMT)" />
          <meta property="og:image" content="https://www.houseologylive.co.uk/static/images/Houseology-live-crew.jpg" />
          <meta property="og:url" content="https://www.houseologylive.co.uk/live-stream" />

          <meta name="twitter:card" content="summary_large_image" />
          <meta name="twitter:site" content="@houseology3" />
          <meta name="twitter:title" content="Houseology Live - Sundays 6pm till 10pm" />
          <meta name="twitter:description" content="A musical collaboration of four digital music artist, DJs and Producers. MiKE RiCHARDS, The ScullyMan, S.Key & Pro-lifik. Live Sundays 6pm till 10pm (GMT)" />
          <meta name="twitter:image" content="https://www.houseologylive.co.uk/static/images/Houseology-live-crew.jpg" />
          <meta name="twitter:image:alt" content="Houseology Live - Sundays 6pm till 11pm (GMT) with MiKE RiCHARDS, The ScullyMan, S.Key & Pro-lifik" />

          <title>{this.props.title || 'Houseology Live'}</title>
          <style dangerouslySetInnerHTML={{__html: Styles}}/>

          <script src="https://cdn.polyfill.io/v2/polyfill.min.js"/>

        </Head>
        <Navbar light className="navbar">
          <Container fluid={this.props.fluid}>
            <Link prefetch href="/">
              <NavbarBrand href="/">
                <div className="logo-holder rounded-circle"><img src="/static/images/Houseology-Live-Logo.svg" style={{width: '100%', paddingRight: '5px', paddingLeft: '5px'}} /></div> Houseology Live
              </NavbarBrand>
            </Link>
            <input className="nojs-navbar-check" id="nojs-navbar-check" type="checkbox" aria-label="Menu"/>
            <label tabIndex="1" htmlFor="nojs-navbar-check" className="nojs-navbar-label mt-2" />
            <div className="nojs-navbar">
              <Nav navbar className="navbar-expand">
                <NavItem>
                  <Link href="/about-us">
                    <a className="btn">About Us</a>
                  </Link>
                </NavItem>

                <NavItem>
                  <Link href="/live-stream">
                    <a className="btn">Live Stream</a>
                  </Link>
                </NavItem>

                <NavItem>
                  <Link href="/podcasts">
                    <a className="btn">Podcasts</a>
                  </Link>
                </NavItem>

                <NavItem>
                  <Link href="/events">
                    <a className="btn">Events</a>
                  </Link>
                </NavItem>

                <NavItem>
                    <a href="https://shop.spreadshirt.co.uk/houseology-live" className="btn" target="_blank">Official Store</a>
                </NavItem>

                <NavItem>
                    <a href="https://www.patreon.com/bePatron?u=19102988" data-patreon-widget-type="become-patron-button">< img src="../static/images/become_a_patron.png" width="30%" style={{margin: '6px 0 10px 0'}} /></a>
                </NavItem>
              </Nav>
              <UserMenu session={this.props.session} toggleModal={this.toggleModal} signinBtn={this.props.signinBtn}/>
            </div>
            </Container>
        </Navbar>

        <NProgress
          color="#FF8C00"
          options={{ trickleSpeed: 50, showSpinner: false }}
        />

        <MainBody navmenu={this.props.navmenu} fluid={this.props.fluid} container={this.props.container}>
          {this.props.children}
        </MainBody>
        <Container fluid={this.props.fluid}>
          <hr className="mt-3"/>

          <Row>
            <Col sm={12} md={4}>
              <p className="text-muted small">
                <a className="text-muted font-weight-bold">{Package.name} {Package.version}</a>
                <span className="ml-2">&copy; {new Date().getYear() + 1900}. All Rights &amp; recordings reserved</span>
              </p>
            </Col>

            <Col sm={12} md={8}>
              <Row>
                <Col sm={12} md={6} className="text-center mb-3">
                  <p className="text-muted font-weight-bold mb-2">Connect with us</p>
                  <Row>
                    <Col><a href="https://www.facebook.com/HouseologyLive/" target="_blank" title="Facebook"><span className="icon ion-logo-facebook"></span></a></Col>
                    <Col><a href="https://twitter.com/houseology3" target="_blank" title="Twitter"><span className="icon ion-logo-twitter"></span></a></Col>
                    <Col><a href="https://tunein.com/podcasts/House/Houseology-Live-TV-Podcast-p1004303/" target="_blank" title="TuneIn"><span className="icon ion-md-radio"></span></a></Col>
                    <Col><a href="https://itunes.apple.com/gb/podcast/houseology-podcast/id1156287748?mt=2" target="_blank" title="iTunes"><span className="icon ion-md-musical-note"></span></a></Col>
                  </Row>
                </Col>

                <Col sm={12} md={6} className="text-center">
                <p className="text-muted font-weight-bold mb-0">Important Stuff</p>
                  <Link href="/privacy-policy">
                      <a className="btn">Privacy Policy</a>
                  </Link>

                  <Link href="/service-terms">
                      <a className="btn">Terms of Service</a>
                  </Link>
                </Col>
              </Row> 
            </Col>
          </Row>
        </Container>
        <SigninModal modal={this.state.modal} toggleModal={this.toggleModal} session={this.props.session} providers={this.state.providers}/>
      </React.Fragment>
    )
  }
}

export class MainBody extends React.Component {
  render() {
    if (this.props.container === false) {
      return (
        <React.Fragment>
          {this.props.children}
        </React.Fragment>
      )
    } else if (this.props.navmenu === false) {
      return (
        <Container fluid={this.props.fluid} style={{marginTop: '1em'}}>
          {this.props.children}
        </Container>
      )
    } else {
      return (
        <Container fluid={this.props.fluid} style={{marginTop: '1em'}}>
          <Row>
            <Col xs="12" md="9" lg="10">
              {this.props.children}
            </Col>
            <Col xs="12" md="3" lg="2" style={{paddingTop: '1em'}}>
              <Nav navbar className="navbar-expand">
                <NavItem>
                  <Link href="/about-us">
                    <a className="btn">About Us</a>
                  </Link>
                </NavItem>

                <NavItem>
                  <Link href="/live-stream">
                    <a className="btn">Live Stream</a>
                  </Link>
                </NavItem>

                <NavItem>
                  <Link as="/houseology/podcasts" href="/podcasts">
                    <a className="btn">Podcasts</a>
                  </Link>
                </NavItem>

                <NavItem>
                  <Link as="/houseology/events" href="/events">
                    <a className="btn">Events</a>
                  </Link>
                </NavItem>
              </Nav>
            </Col>
          </Row>
        </Container>
      )
    }
  }
}

export class UserMenu extends React.Component {
  constructor(props) {
    super(props)
    this.handleSignoutSubmit = this.handleSignoutSubmit.bind(this)
  }

   async handleSignoutSubmit(event) {
     event.preventDefault()
     
     // Save current URL so user is redirected back here after signing out
     const cookies = new Cookies()
     cookies.set('redirect_url', window.location.pathname, { path: '/' })

     await NextAuth.signout()
     Router.push('/')
   }
   
  render() {
    if (this.props.session && this.props.session.user) {
      // If signed in display user dropdown menu
      const session = this.props.session
      return (
        <Nav className="ml-auto" navbar>
          {/*<!-- Uses .nojs-dropdown CSS to for a dropdown that works without client side JavaScript ->*/}
          <div tabIndex="2" className="dropdown nojs-dropdown">
            <div className="nav-item">
              <span className="dropdown-toggle nav-link d-none d-md-block">
                <span className="icon ion-md-contact" style={{fontSize: '2em', position: 'absolute', top: -5, left: -37}}></span>
              </span>
              <span className="dropdown-toggle nav-link d-block d-md-none">
                <span className="icon ion-md-contact mr-2"></span>
                {session.user.name || session.user.email}
              </span>
            </div>
            <div className="dropdown-menu">
              <Link prefetch href="/user/account">
                <a href="/user/account" className="dropdown-item"><span className="icon ion-md-person mr-1"></span> Your Account</a>
              </Link>
              <AdminMenuItem {...this.props}/>
              <div className="dropdown-divider d-none d-md-block"/>
              <div className="dropdown-item p-0">
                <Form id="signout" method="post" action="/auth/signout" onSubmit={this.handleSignoutSubmit}>
                  <input name="_csrf" type="hidden" value={this.props.session.csrfToken}/>
                  <Button type="submit" block className="pl-4 rounded-0 text-left dropdown-item"><span className="icon ion-md-log-out mr-1"></span> Sign out</Button>
                </Form>
              </div>
            </div>
          </div>
        </Nav>
      )
     } if (this.props.signinBtn === false) {
       // If not signed in, don't display sign in button if disabled
      return null
    } else {
      // If not signed in, display sign in button
      return (
        <Nav className="ml-auto" navbar>
          <NavItem>
            {/**
              * @TODO Add support for passing current URL path as redirect URL
              * so that users without JavaScript are also redirected to the page
              * they were on before they signed in.
              **/}
            <a href="/auth?redirect=/" className="btn btn-outline-primary" onClick={this.props.toggleModal}><span className="icon ion-md-log-in mr-1"></span> Sign up / Sign in</a>
          </NavItem>
        </Nav>
      )
    }
  }
}

export class AdminMenuItem extends React.Component {
  render() {
    if (this.props.session.user && this.props.session.user.admin === true) {
      return (
        <React.Fragment>
          <Link prefetch href="/admin/admin">
            <a href="/admin/admin" className="dropdown-item"><span className="icon ion-md-settings mr-1"></span> Admin</a>
          </Link>
        </React.Fragment>
      )
    } else {
      return(<div/>)
    }
  }
}

export class SigninModal extends React.Component {
  render() {
    if (this.props.providers === null) return null
    
    return (
      <Modal isOpen={this.props.modal} toggle={this.props.toggleModal} style={{maxWidth: 700}}>
        <ModalHeader>Sign up / Sign in</ModalHeader>
        <ModalBody style={{padding: '1em 2em'}}>
          <Signin session={this.props.session} providers={this.props.providers}/>
        </ModalBody>
      </Modal>
    )
  }
}