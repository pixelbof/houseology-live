import React from 'react'
import io from 'socket.io-client'
import { Row, Col, Modal, ModalHeader, ModalBody } from 'reactstrap'
import Moment from 'react-moment'
import GiphyPicker from 'react-giphy-component'
import { EmojioneV4 } from 'react-emoji-render';
import EmojiPicker from 'emoji-picker-react'
import 'emoji-picker-react/dist/universal/style.scss'

export default class Chat extends React.Component {
    constructor(props) {
        super(props);
        const user = (this.props.session && this.props.session.user) ? this.props.session.user.name : 'Guest' + Math.floor(Math.random() * 10000000);

        this.state = {
            username: user,
            message: '',
            messages: [],
            giphyModal: false,
            emojiModal: false,
            baleModal: false,
            registeredCount: 0,
            guestCount: 0
        };

        this.toggleGiphyModal = this.toggleGiphyModal.bind(this);
        this.toggleEmojiModal = this.toggleEmojiModal.bind(this);
        this.toggleBaleModal = this.toggleBaleModal.bind(this);

        this.getTime = () => {
            const date = new Date();
            return date.getTime();
        }

        this.isShowTime = () => {
            const date = new Date();
            return (date.getDay() === 0);
        }

        this.checkMessageArr = () => {
            if (this.state.message.length >= 10) {
                while(this.state.message.length > 10)
                    this.state.message.shift(); 
            }
        }

        this.handleEmojiClick = (code, emoji) => {
            this.setState({message: `${this.state.message} :${emoji.name}:`});
        }

        this.handleGiphyClick = (giphy) => {
            this.sendBale(giphy.downsized.url);

            this.setState(prevState => ({
                giphyModal: !prevState.giphyModal
            }));
        }

        this.handleBaleClick = (ev) => {
            ev.preventDefault();
            const _bale = ev.target;

            this.sendBale(`../static/bales/${_bale.dataset.bale}-bale.gif`);

            this.setState(prevState => ({
                baleModal: !prevState.baleModal
            }));
        }

        this.socket = io('https://houseologychat.herokuapp.com', {transports: ['websocket'], upgrade: false});
        //this.socket = io('http://localhost:3001', {transports: ['websocket'], upgrade: false});
        this._addMsg = data => { addMessage(data); };

        //socket FROM server
        this.socket.on('RECEIVE_MESSAGE', (data) =>{
            addMessage(data);
        });

        this.socket.on('USER_COUNT_UPDATE', (data) => {
            this.setState({registeredCount: data.registered, guestCount: data.guest});
        })

        const addMessage = data => {
            this.checkMessageArr();
            this.setState({messages: [...this.state.messages, data]});
        };

        //socket TO server
        this.welcome = (user) => {
            let guest = false;
            (user.includes('Guest')) ? guest = true : guest = false;
            this.socket.emit('WELCOME', {user: user, guest: guest, timeSent: this.getTime()}); 
        }

        this.sendBale = (bale) => {
            this.socket.emit('SEND_MESSAGE', {
                author: this.state.username,
                bale: true,
                message: bale,
                timeSent: this.getTime()
            });
        }

        this.sendMessage = ev => {
            ev.preventDefault();
            this.socket.emit('SEND_MESSAGE', {
                author: this.state.username,
                bale: false,
                message: this.state.message,
                timeSent: this.getTime()
            });

            this.setState({message: ''});
        }

    }

    componentDidMount() {
        const curTime = this.getTime();
        const _message = (this.isShowTime()) ? 'chat available' : 'chat available, but show is not live, people may not be logged in';

        const playerHeight = document.getElementById('videoHolder').clientHeight - 144;

        document.getElementById('message-body').style.height = (playerHeight > 100) ? `${playerHeight}px` : '250px';

        this.welcome(this.state.username);
        this._addMsg({author: 'System', message: _message, timeSent: curTime});

        // Get the input field
        var input = document.getElementById("msgHolder");

        // Execute a function when the user releases a key on the keyboard
        input.addEventListener("keyup", function(event) {
        // Number 13 is the "Enter" key on the keyboard
        if (event.keyCode === 13) document.getElementById("sendMsg").click();
        });
    }

    componentDidUpdate () {
        this.scrollToBottom();
    }
    componentWillUnmount() {
        this.socket.close();
    }

    scrollToBottom = () => {
        const elem = document.getElementById('message-body');
        elem.scrollTop = elem.scrollHeight;
    }

    toggleGiphyModal() {
        this.setState(prevState => ({
            giphyModal: !prevState.giphyModal
        }));
    }

    toggleEmojiModal() {
        this.setState(prevState => ({
            emojiModal: !prevState.emojiModal
        }));
    }

    toggleBaleModal() {
        this.setState(prevState => ({
            baleModal: !prevState.baleModal
        }));
    }

    render() {
        return (
            <Col md={4}>
                <div className="card">
                    <Row style={{margin: '0'}}>
                        <Col sm="12" className="card-body" id="message-body">
                            <div className="messages">
                                {this.state.messages.map(message => {
                                    return (
                                        <div key={Math.random() * 10} className={message.author === this.state.username ? 'myMessage' : 'otherMessage'}>
                                            <div className="lead">{message.author}</div> 
                                            { (!message.bale) ? <div><EmojioneV4  text={message.message} /></div> : <div className="text-center"><img src={message.message} width={"200px"} height={"auto"} style={{backgroundColor: "#FF4500"}} /></div>}
                                            <div className="text-right font-weight-light font-italic timeSent"><Moment fromNow>{message.timeSent}</Moment></div>
                                        </div>
                                    )
                                })}
                            </div>
                        </Col>
                    </Row>

                    <Row style={{margin: '0'}}>
                        <Col xs={12} style={{padding:'0'}}>
                            <div className="card-footer">
                                <input type="hidden" placeholder="Username" value={this.state.username} onChange={ev => this.setState({username: ev.target.value})} className="form-control"/>
                                <br/>
                                <div style={{position:'relative'}}>
                                <input type="text" placeholder="Message" id="msgHolder" className="form-control" style={{padding: '10px 90px 10px 10px'}} value={this.state.message} onChange={ev => this.setState({message: ev.target.value})}/>

                                <button className="btn btn-secondary" type="button" style={{position:'absolute',right:'0', top:'0'}} onClick={this.toggleEmojiModal}>
                                    <span className="icon ion-md-happy"></span>
                                </button>

                                <button className="btn btn-secondary" type="button" style={{position:'absolute',right:'40px', top:'0'}} onClick={this.toggleGiphyModal}>
                                    <span className="icon ion-md-images"></span>
                                </button>

                                <Modal isOpen={this.state.giphyModal} toggle={this.toggleGiphyModal} className={this.props.className}>
                                    <ModalHeader toggle={this.toggleGiphyModal}>Giphy's, make chatting fun!</ModalHeader>

                                    <ModalBody>
                                        <GiphyPicker apiKey={"9QwFBO5rsymRJjqU7BRWchzDB8BAebQx"} imagePlaceholderColor={"#FF4500"} onSelected={this.handleGiphyClick.bind(this)} style={{width:'100%'}} />
                                    </ModalBody>
                                </Modal>

                                <Modal isOpen={this.state.emojiModal} toggle={this.toggleEmojiModal} className={this.props.className}>
                                    <ModalHeader toggle={this.toggleEmojiModal}>Emoji's, for when words are just not enough!</ModalHeader>

                                    <ModalBody>
                                        {<EmojiPicker onEmojiClick={this.handleEmojiClick.bind(this)} />}
                                    </ModalBody>
                                </Modal>

                                <Modal isOpen={this.state.baleModal} toggle={this.toggleBaleModal} className={this.props.className}>
                                    <ModalHeader toggle={this.toggleBaleModal}>Bales, show us you love us!</ModalHeader>

                                    <ModalBody>
                                        <Row>
                                            <Col><p class="lead">MiKE RiCHARDS</p><img src="../static/bales/mike-bale.gif" onClick={this.handleBaleClick} data-bale='mike' style={{cursor:'pointer'}} /></Col>
                                            <Col><p class="lead">DJ ScullyMan</p><img src="../static/bales/scully-bale.gif" onClick={this.handleBaleClick} data-bale='scully' style={{cursor:'pointer'}} /></Col>
                                            <Col><p class="lead">DJ Reece Richards</p><img src="../static/bales/reece-bale.gif" onClick={this.handleBaleClick} data-bale='reece' style={{cursor:'pointer'}} /></Col>
                                        </Row>
                                        <Row>
                                            <Col><p class="lead">DJ Pro-lifik</p><img src="../static/bales/prolifik-bale.gif" onClick={this.handleBaleClick} data-bale='prolifik' style={{cursor:'pointer'}} /></Col>
                                            <Col><p class="lead">DJ S.Key</p><img src="../static/bales/skey-bale.gif" onClick={this.handleBaleClick} data-bale='skey' style={{cursor:'pointer'}} /></Col>
                                            <Col><p class="lead">V.T Engineer Malroy</p><img src="../static/bales/malroy-bale.gif" onClick={this.handleBaleClick} data-bale='malroy' style={{cursor:'pointer'}} /></Col>
                                        </Row>
                                        <Row>
                                            <Col><p class="lead">Guest DJ</p><img src="../static/bales/guest-bale.gif" onClick={this.handleBaleClick} data-bale='guest' style={{cursor:'pointer'}} /></Col>
                                        </Row>
                                    </ModalBody>
                                </Modal>

                                </div>
                                
                                <Row style={{margin:"20px 0 0 0"}}>
                                    <Col xs={6}>
                                        <button onClick={this.sendMessage} id="sendMsg" className="btn btn-primary form-control">Send</button>
                                    </Col>

                                    <Col xs={6}>
                                        <button className="btn btn-primary form-control" type="button" onClick={this.toggleBaleModal}>
                                            Send Bales
                                        </button>
                                    </Col> 
                                </Row>
                            </div>   
                        </Col>
                    </Row>
                </div>
            </Col>
        )
    }
}