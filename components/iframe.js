import React from 'react';

export default class IframeComponent extends React.Component {
  render() {
    return (
      <script type="text/javascript" src="//player.wowza.com/player/latest/wowzaplayer.min.js"></script>

<div id="playerElement" class="embed-responsive embed-responsive-16by9"></div>

<script type="text/javascript">
WowzaPlayer.create("playerElement",{
"license":"PLAY1-eBebJ-RkdZy-N66R4-kKJan-UdUwW",
"title":"",
"description":"",
"sourceURL":"http%3A%2F%2Fhouseologylive.flashmediacast.com%3A1935%2Fhouseologylive%2Flivestream%2Fplaylist.m3u8",
"autoPlay":false,
"volume":"75",
"mute":false,
"loop":false,"audioOnly":false,"uiShowQuickRewind":true,"uiQuickRewindSeconds":"30"});</script>
    )
  }
}